package com.hmall.common.client;

import com.hmall.common.dto.Item;
import com.hmall.common.dto.PageDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;

//指定远程调用哪一个微服务
@FeignClient("itemservice")
public interface ItemClient {

    @GetMapping("/item/list")//?page=10&size=10, Controller可以省略@RequestParam
    PageDTO<Item> queryItemByPage(@RequestParam Integer page, @RequestParam Integer size);


    //=============================新增接口
    @GetMapping("/item/{id}")
    Item queryItemById(@PathVariable("id") Long id);

    //ItemClient
    @PutMapping("/item/stock/{itemId}/{num}")
    void updateStock(@PathVariable("itemId") Long itemId,
                     @PathVariable("num") Integer num);
}
