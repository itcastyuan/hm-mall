package com.hmall.item.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hmall.item.pojo.Item;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

public interface ItemMapper extends BaseMapper<Item> {

    @Update("update tb_item set stock = stock + #{num} where id = #{itemId}")
    void updateStock(@Param("itemId") Long itemId, @Param("num") Integer num);
}
