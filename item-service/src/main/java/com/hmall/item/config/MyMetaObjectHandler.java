package com.hmall.item.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

@Slf4j
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("start insert fill ....");
        //metaObject 新增商品的对象：item
        this.strictInsertFill(metaObject,
                "createTime", Date.class, new Date()); // 起始版本 3.3.0(推荐使用)

        this.strictInsertFill(metaObject,
                "updateTime", Date.class, new Date()); // 起始版本 3.3.0(推荐使用)
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("start update fill ....");
        this.strictUpdateFill(metaObject,
                "updateTime", Date.class, new Date()); // 起始版本 3.3.0(推荐使用)
    }
}