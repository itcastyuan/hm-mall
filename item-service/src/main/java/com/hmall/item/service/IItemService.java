package com.hmall.item.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hmall.item.pojo.Item;

public interface IItemService extends IService<Item> {

    //Item selectByID(Long id);

    //接口：IItemService
    void deductStock(Long itemId, Integer num);
}
