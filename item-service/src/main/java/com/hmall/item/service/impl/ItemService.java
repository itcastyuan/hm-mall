package com.hmall.item.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hmall.item.mapper.ItemMapper;
import com.hmall.item.pojo.Item;
import com.hmall.item.service.IItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ItemService extends ServiceImpl<ItemMapper, Item> implements IItemService {

    //@Autowired
    //private ItemMapper itemMapper;
    //
    //@Override
    //public Item selectByID(Long id) {
    //    return itemMapper.selectById(id);
    //}

    //实现类：ItemService
    @Autowired
    private ItemMapper itemMapper;

    @Override
    public void deductStock(Long itemId, Integer num) {
        try {

            //1.根据id更新
            //itemMapper.updateById();

            //2.根据指定的条件更新：LambdaUpdateWrapper
            //Wrappers.<Item>lambdaUpdate()
            //        .eq(Item::getId, itemId)
            //        .set(Item::getStock, Item::getStock + 1);

            //先判断库存是否够
            itemMapper.updateStock(itemId, num);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("库存不足！");
        }
    }
}
