package com.hmall.item;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hmall.item.pojo.Item;
import com.hmall.item.service.IItemService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class ItemServiceTest {

    @Autowired
    private IItemService itemService;


    //数据层（数据库）: insert, update, delete, select
    //业务层：         save,   update, remove, get


    @Test
    public void testPage() {
        //1. 产生对象：添加分页拦截器

        //2. 执行真正分页查询
        IPage<Item> page = new Page<>(1, 10);
        itemService.page(page);
        //3. 获取分页结果
        long total = page.getTotal();
        List<Item> records = page.getRecords();
    }

    @Test
    public void testGetById() {
        Item item = itemService.getById(1L);
        itemService.save(item);

        itemService.removeById(1L);
    }

    @Test
    public void testUpdateStatus() {
        //1.只修改部分字段：status, 前端只会传递商品ID
        LambdaUpdateWrapper<Item> uw = new LambdaUpdateWrapper<>();
        //set status = ? where id = ?
        uw.eq(Item::getId, 1L).set(Item::getStatus, 2);
        itemService.update(uw);
    }

    @Test
    public void testUpdate() {
        Item item = itemService.getById(1L);

        //2.修改比较多的字段
        itemService.updateById(item);
    }

}
