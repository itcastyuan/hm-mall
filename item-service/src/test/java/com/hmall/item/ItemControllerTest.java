package com.hmall.item;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hmall.common.dto.PageDTO;
import com.hmall.item.mapper.ItemMapper;
import com.hmall.item.pojo.Item;
import com.hmall.item.service.IItemService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.List;

@SpringBootTest
public class ItemControllerTest {

    @Autowired
    private ItemMapper itemMapper;

    @Autowired
    private IItemService itemService;

    //表现层-》业务层-》数据层

    @Test
    public void testSelectById() {
        //Item item = itemMapper.selectById(584394);
        //System.out.println(item);

        Item item = itemService.getById(100002672306L);
        System.out.println(item);
    }

    @Test
    public void testPage() {
        int page=1, size=5;
        //1.构建分页查询条件
        IPage<Item> itemPage = new Page<>(page, size);
        //2.执行分页查询
        itemService.page(itemPage, null);

        //3.获取查询之后的数据
        List<Item> list = itemPage.getRecords();
        long total = itemPage.getTotal();
        PageDTO<Item> pageDTO = new PageDTO<>(total, list);
        System.out.println(pageDTO);
    }

    @Test
    public void testSave() {
        Item item = itemService.getById(317578L);

        Item newItem = new Item();
        BeanUtils.copyProperties(item, newItem,
                "id", "createTime", "updateTime");

        newItem.setCreateTime(new Date());
        itemService.save(newItem); //要执行填充吗
        System.out.println("新增成功：" + newItem);
    }

    @Test
    public void testUpdate() {
        Long id = 317578L;
        int status = 2;

        //1.通过实体类进行更新
        Item item = new Item();
        item.setId(id);
        item.setStatus(status);
        //如果按照主键进行更新
        itemService.updateById(item);

        //2.直接指定要修改什么，指定根据什么来修改
        //LambdaQueryWrapper<Item> qw = new LambdaQueryWrapper<>();
        //LambdaQueryWrapper<Item> qw = Wrappers.lambdaQuery();
        //LambdaUpdateWrapper<Item> uw = new LambdaUpdateWrapper<>();
        ////指定修改什么
        //uw.set(Item::getStatus, status); //set status = ?
        ////指定根据什么修改
        //uw.eq(Item::getId, id); //where id = ?
        //itemService.update(uw);

        //链式编程简化代码
        //itemService.update(
        //        Wrappers.<Item>lambdaUpdate() // === = new LambdaUpdateWrapper<>();
        //        .set(Item::getStatus, status)
        //        .eq(Item::getId, id));

        System.out.println(itemService.getById(317578L));
    }


    @Test
    public void testDelete() {
        itemService.removeById(23423);
    }
}
