package com.hmall.order.web;

import com.hmall.order.pojo.Order;
import com.hmall.order.pojo.RequestParams;
import com.hmall.order.service.IOrderService;
import com.hmall.order.utils.BaseContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/order")
public class OrderController {

   @Autowired
   private IOrderService orderService;

   @GetMapping("{id}")
   public Order queryOrderById(@PathVariable("id") Long orderId) {
      return orderService.getById(orderId);
   }

   @GetMapping("/hi")
   public String hi() {
      //从ThreadLocal中获取用户ID
      Long userId = BaseContext.getCurrentId();
      System.err.println(userId);
      return "hi" + userId;
   }

   @PostMapping
   public Order createOrder(@RequestBody RequestParams requestParams){
      //返回订单
      //return null;

      return orderService.createOrder(requestParams);
   }
}
