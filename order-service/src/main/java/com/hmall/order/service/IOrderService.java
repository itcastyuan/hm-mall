package com.hmall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hmall.order.pojo.Order;
import com.hmall.order.pojo.RequestParams;

public interface IOrderService extends IService<Order> {

    Order createOrder(RequestParams requestParams);

    //接口：IOrderService
    //处理未支付订单
    void dealExpireOrder(long orderId);
}
