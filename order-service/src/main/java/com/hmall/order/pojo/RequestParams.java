package com.hmall.order.pojo;

import lombok.Data;

@Data
//OrderDTO
public class RequestParams {
    private Integer num; //购买数量
    private Long itemId; //商品ID
    private Long addressId;//配送地址
    private Integer paymentType; //支付方式
}