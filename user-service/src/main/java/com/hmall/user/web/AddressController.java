package com.hmall.user.web;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.hmall.user.pojo.Address;
import com.hmall.user.service.IAddressService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/address")
public class AddressController {

    @Autowired
    private IAddressService addressService;

    @GetMapping("/uid/{userId}")
    public List<Address> findAddressByUserId(@PathVariable("userId") Long userId) {

        //select * from tb_address where user_id =?
        return addressService.list(
                //new LambdaQueryWrapper<Address>().eq(Address::getUserId, userId)
                Wrappers.<Address>lambdaQuery().eq(Address::getUserId, userId)
        );
    }

    @GetMapping("/{addressId}")
    public Address findAddressById(@PathVariable("addressId") Long addressId) {
        return addressService.getById(addressId);
    }

}
