package com.hmal.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 地址
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Address implements Cloneable, Serializable {

    private String city;
    private String country;

    // constructors, getters and setters

}