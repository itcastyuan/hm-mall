import com.fasterxml.jackson.databind.ObjectMapper;
import com.hmal.pojo.Address;
import com.hmal.pojo.User;
import org.apache.commons.lang.SerializationUtils;
import org.junit.jupiter.api.Test;

import java.io.IOException;

public class CopyTest {

    @Test
    public void testShallowCopy() {

        Address address = new Address("杭州", "中国");
        User user = new User("大山", address);

        User copyUser = (User) user.clone();

        // 修改源对象的值
        user.getAddress().setCity("深圳");

        System.out.println(user.getAddress().getCity());
        System.out.println(copyUser.getAddress().getCity());

        //修改原始对象的address属性值
        System.out.println("拷贝之后对象：" + (user.getAddress() == copyUser.getAddress()));
    }

    @Test
    public void serializableCopy() {

        Address address = new Address("杭州", "中国");
        User user = new User("大山", address);

        // 使用Apache Commons Lang序列化进行深拷贝
        //1.user对象-》json字符串 2.json字符串-》新的对象clone
        User copyUser = (User) SerializationUtils.clone(user);

        // 修改源对象的值
        user.getAddress().setCity("深圳");

        System.out.println(user.getAddress().getCity()); //深圳
        System.out.println(copyUser.getAddress().getCity()); //杭州

        //修改原始对象的address属性值: false
        System.out.println("拷贝之后对象：" + (user.getAddress() == copyUser.getAddress()));

    }

    @Test
    public void jacksonCopy() throws IOException {

        Address address = new Address("杭州", "中国");
        User user = new User("大山", address);

        // 使用Jackson序列化进行深拷贝
        ObjectMapper objectMapper = new ObjectMapper();
        User copyUser = objectMapper.readValue(objectMapper.writeValueAsString(user), User.class);

        // 修改源对象的值
        user.getAddress().setCity("深圳");

        System.out.println(user.getAddress().getCity());
        System.out.println(copyUser.getAddress().getCity());

        //修改原始对象的address属性值
        System.out.println("拷贝之后对象：" + (user.getAddress() == copyUser.getAddress()));

    }
}
