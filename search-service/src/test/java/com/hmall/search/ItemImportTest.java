package com.hmall.search;


import com.alibaba.fastjson.JSON;
import com.hmall.common.client.ItemClient;
import com.hmall.common.dto.Item;
import com.hmall.common.dto.PageDTO;
import com.hmall.search.pojo.ItemDoc;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.util.List;

@SpringBootTest
public class ItemImportTest {

    @Autowired
    private ItemClient itemClient;
    //编写一个单元测试完成
    //伪代码
    @Autowired
    private RestHighLevelClient restHighLevelClient;

    @Test //注意导包，用长的那个：import org.junit.jupiter.api.Test;
    public void testQueryItem() throws IOException {
        // http://itemservice/item/list?page=1&size=5
        int page = 1, size = 100;

        while (true) {
            //远程调用商品微服务进行分页查询：
            PageDTO<Item> pageDTO = itemClient.queryItemByPage(page, size);
            List<Item> list = pageDTO.getList();
            if (CollectionUtils.isEmpty(list)) {
                break;
            }
            // 1.准备BulkRequest
            BulkRequest request = new BulkRequest();
            // 2.准备DSL
            // 遍历
            for (Item item : list) {
                if(item.getStatus() == 2){
                    // 下架商品直接跳过
                    continue;
                }
                // 把 Item 转为 ItemDoc
                ItemDoc itemDoc = new ItemDoc(item);
                //将itemDoc使用jackson转成json数据
                String json = JSON.toJSONString(itemDoc);
                // 添加新增请求
                request.add(new IndexRequest("item")
                        .id(itemDoc.getId().toString())
                        .source(json, XContentType.JSON)
                );
            }
            // 3.发请求，批量处理
            restHighLevelClient.bulk(request, RequestOptions.DEFAULT);
            page++;
        }
    }

    @Test
    void testAgg() throws IOException {
        // 1.准备请求
        SearchRequest request = new SearchRequest("item");
        // 2.请求参数
        // 2.1.size
        request.source().size(0); //不用文档数据，只要聚合结果
        // 2.2.聚合
        request.source().aggregation(
                AggregationBuilders.terms("brandAgg").field("brand").size(20));
        // 3.发出请求
        SearchResponse response =
                restHighLevelClient.search(request, RequestOptions.DEFAULT);
        System.out.println("查询结果：" + response);

        // 4.解析结果
        Aggregations aggregations = response.getAggregations();

        // 4.1.根据聚合名称，获取聚合结果
        Terms brandAgg = aggregations.get("brandAgg");

        // 4.2.获取buckets
        // 4.3.遍历
        for (Terms.Bucket bucket : brandAgg.getBuckets()) {
            String brandName = bucket.getKeyAsString();
            System.out.println("brandName = " + brandName);
            long docCount = bucket.getDocCount();
            System.out.println("docCount = " + docCount);
        }
    }
}
