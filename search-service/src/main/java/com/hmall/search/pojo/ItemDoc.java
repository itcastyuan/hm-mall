package com.hmall.search.pojo;

import com.hmall.common.dto.Item;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ItemDoc {
    private Long id;
    private String name;
    private Long price;
    private String image;
    private String category;
    private String brand;
    private Integer sold;
    private Integer commentCount;
    private Boolean isAD;

    public ItemDoc(Item item) {
        //属性拷贝BeanUtils（底层用的是反射）
        //BeanUtils.copyProperties(item, this);
        this.id = item.getId();
        this.name = item.getName();
        this.price = item.getPrice();
        this.image = item.getImage();
        this.category = item.getCategory();
        this.brand = item.getBrand();
        this.sold = item.getSold();
        this.commentCount = item.getCommentCount();
        this.isAD = item.getIsAD();
    }
}